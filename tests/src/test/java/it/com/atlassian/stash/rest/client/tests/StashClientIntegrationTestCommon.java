package it.com.atlassian.stash.rest.client.tests;

import com.atlassian.stash.rest.client.api.AvatarRequest;
import com.atlassian.stash.rest.client.api.EntityMatchers;
import com.atlassian.stash.rest.client.api.StashClient;
import com.atlassian.stash.rest.client.api.StashVersions;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.PullRequestRef;
import com.atlassian.stash.rest.client.api.entity.PullRequestStatus;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.Tag;
import com.google.common.collect.ImmutableList;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import static com.atlassian.stash.rest.client.api.EntityMatchers.page;
import static com.atlassian.stash.rest.client.api.EntityMatchers.project;
import static com.atlassian.stash.rest.client.api.EntityMatchers.pullRequestParticipant;
import static com.atlassian.stash.rest.client.api.EntityMatchers.pullRequestRef;
import static com.atlassian.stash.rest.client.api.EntityMatchers.pullRequestStatus;
import static com.atlassian.stash.rest.client.api.EntityMatchers.repository;
import static com.atlassian.stash.rest.client.api.StashClient.PullRequestDirection.OUTGOING;
import static com.atlassian.stash.rest.client.api.StashClient.PullRequestStateFilter.OPEN;
import static com.atlassian.stash.rest.client.api.StashClient.PullRequestsOrder.NEWEST;
import static com.atlassian.stash.rest.client.api.StashVersions.RENAMED_TO_BITBUCKET;
import static it.com.atlassian.stash.rest.client.tests.TestUtil.STASH_ADMIN_LOGIN;
import static it.com.atlassian.stash.rest.client.tests.TestUtil.STASH_ADMIN_PASSWORD;
import static java.lang.Boolean.TRUE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public abstract class StashClientIntegrationTestCommon {
    public static final Function<Branch, String> STASH_BRANCH_ENTITY_TO_NAME = Branch::getDisplayId;
    public static final Function<Tag, String> STASH_TAG_ENTITY_TO_NAME = Tag::getDisplayId;
    protected StashClient service;
    protected ForkedRepoPRFixture forkedRepoPRFixture;
    /**
     * either </code>/stash</code> or <code>/bitbucket</code>, depending on the product version being tested
     */
    protected String appContext;
    /**
     * dynamically retrieved version of the Stash/Bitbucket being tested
     */
    private String buildNumber;

    protected static <T> Matcher<Iterable<? extends T>> hasOnlyItem(Matcher<T> itemMatcher) {
        return new BaseMatcher<Iterable<? extends T>>() {
            String error;

            @Override
            public boolean matches(Object o) {
                if (!(o instanceof Iterable)) {
                    error = "expected Iterable, got " + ((null == o) ? "null" : o.getClass());
                    return false;
                }
                //noinspection unchecked
                final Iterator<T> iterator = ((Iterable) o).iterator();
                if (!iterator.hasNext()) {
                    error = "got empty collection";
                    return false;
                }
                final T first = iterator.next();
                if (!itemMatcher.matches(first)) {
                    error = "element mismatches";
                    return false;
                }
                if (iterator.hasNext()) {
                    error = "more than 1 element found, first unexpected: " + iterator.next();
                    return false;
                }
                return true;

            }

            @Override
            public void describeTo(Description description) {
                description.appendText(error).appendText(": ").appendDescriptionOf(itemMatcher);
            }
        };
    }

    protected static Matcher<List<PullRequestStatus>> pullRequestsMatcher(final PullRequestStatus pullRequest) {
        return new BaseMatcher<List<PullRequestStatus>>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("not found pull request with id " + pullRequest.getId());
            }

            @Override
            public boolean matches(Object o) {
                final List<PullRequestStatus> prs = (List<PullRequestStatus>) o;
                return prs.stream().anyMatch(pr -> pr.getId() == pullRequest.getId());
            }
        };
    }

    @Before
    public void setUp() throws Exception {
        service = createStashClient(STASH_ADMIN_LOGIN, STASH_ADMIN_PASSWORD);
        forkedRepoPRFixture = new ForkedRepoPRFixture();
        buildNumber = service.getApplicationProperties().getBuildNumber();
        appContext = RENAMED_TO_BITBUCKET.givenVersionIsOlder(buildNumber) ? "/stash" : "/bitbucket";
    }

    @After
    public void tearDown() {
        service.deleteRepository(forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug);
    }

    abstract protected StashClient createStashClient(String stashUsername, String stashPassword) throws Exception;

    protected void assumeStashVersionAtLeast(final StashVersions expectedStashVersion) {
        Assume.assumeThat(buildNumber, stashVersionAtLeast(expectedStashVersion));
    }

    private Matcher<String> stashVersionAtLeast(final StashVersions expectedVersion) {
        return new BaseMatcher<String>() {
            String value;

            @Override
            public boolean matches(final Object o) {
                if (!(o instanceof String)) {
                    return false;
                }
                final String s = (String) o;
                if (expectedVersion.givenVersionIsOlder(s)) {
                    return false;
                }
                value = s;
                return true;
            }

            @Override
            public void describeTo(final Description description) {
                description.appendText("expected Stash to be at least at version ")
                        .appendValue(buildNumber)
                        .appendText("but found version ")
                        .appendValue(value);
            }
        };
    }

    protected Matcher<String> startsWith(final String prefix) {
        return new BaseMatcher<String>() {
            String error;

            @Override
            public boolean matches(Object o) {
                if (!(o instanceof String)) {
                    error = "Expected String, got " + ((null == o) ? "null" : o.getClass());
                    return false;
                }
                final String s = (String) o;
                if (!s.startsWith(prefix)) {
                    error = "Expected String starting with " + prefix;
                    return false;
                }
                return true;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(error);
            }
        };
    }

    protected Matcher<String> endsWith(String suffix) {
        return new BaseMatcher<String>() {
            String error;

            @Override
            public boolean matches(Object o) {
                if (!(o instanceof String)) {
                    error = "Expected String, got " + ((null == o) ? "null" : o.getClass());
                    return false;
                }
                final String s = (String) o;
                if (!s.endsWith(suffix)) {
                    error = "Expected String ending with " + suffix;
                    return false;
                }
                return true;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(error);
            }
        };
    }

    protected void forkTestRepository() {
        // set up test repository that we can play with
        final Repository repository = service.forkRepository(forkedRepoPRFixture.projectKey,
                forkedRepoPRFixture.sourceRepo, forkedRepoPRFixture.projectKey, forkedRepoPRFixture.forkedRepoSlug);
        assertThat(repository, repository()
                .name(is(forkedRepoPRFixture.forkedRepoName))
                .slug(is(forkedRepoPRFixture.forkedRepoSlug))
                .project(project().
                        key(is(forkedRepoPRFixture.projectKey))
                        .build())
                .origin(repository()
                        .name(is(forkedRepoPRFixture.sourceRepo))
                        .slug(is(forkedRepoPRFixture.sourceRepo))
                        .project(project().
                                key(is(forkedRepoPRFixture.projectKey))
                                .build())
                        .build())
                .build());

        // give user access to test repo so they can participate in PR
        final boolean permGranted = service.addRepositoryUserPermission(forkedRepoPRFixture.projectKey,
                forkedRepoPRFixture.forkedRepoSlug, forkedRepoPRFixture.testUser, Permission.REPO_READ);
        assertThat(permGranted, is(TRUE));
    }

    @Nonnull
    protected List<PullRequestStatus> getPullRequests(@Nullable String branchName, @Nullable StashClient.PullRequestDirection direction,
                                                      @Nullable StashClient.PullRequestStateFilter stateFilter,
                                                      @Nullable StashClient.PullRequestsOrder order) {
        return service.getPullRequestsByRepository(forkedRepoPRFixture.projectKey,
                forkedRepoPRFixture.forkedRepoSlug,
                branchName, direction, stateFilter, order,
                0, 10).getValues();
    }

    class ForkedRepoPRFixture {
        /**
         * fork of rep1 repo used to test pull requests that may leave environment polluted in case of failure.
         * Deleted in {@see #tearDown} because of the above.
         */
        final String forkedRepoSlug = "rep1fork";
        final String forkedRepoName = "rep1fork";
        final String projectKey = "PROJECT_1";
        final String projectName = "Project 1";
        final String sourceRepo = "rep_1";
        final String testUser = "user";
        final String sourceBranch = "refs/heads/basic_branching";
        final String sourceBranchDisplayId = "basic_branching";
        final String targetBranch = "refs/heads/master";
        final String targetBranchDisplayId = "master";
        final PullRequestRef fromRef = new PullRequestRef(forkedRepoSlug, forkedRepoName, projectKey, projectName, sourceBranch);
        final PullRequestRef toRef = new PullRequestRef(forkedRepoSlug, forkedRepoName, projectKey, projectName, targetBranch);
        final EntityMatchers.PullRequestStatusMatcherBuilder prMatcherBuilder = pullRequestStatus()
                .title(is("TEST_PR"))
                .fromRef(pullRequestRef()
                        .displayId(is(sourceBranchDisplayId))
                        .id(is(sourceBranch))
                        .projectKey(is(projectKey))
                        .projectName(is(projectName))
                        .repositorySlug(is(forkedRepoSlug))
                        .repositoryName(is(forkedRepoName))
                        .build())
                .toRef(pullRequestRef()
                        .displayId(is(targetBranchDisplayId))
                        .id(is(targetBranch))
                        .projectKey(is(projectKey))
                        .projectName(is(projectName))
                        .repositorySlug(is(forkedRepoSlug))
                        .repositoryName(is(forkedRepoName))
                        .build())
                .author(pullRequestParticipant()
                        .name(is(STASH_ADMIN_LOGIN))
                        .role(is("AUTHOR"))
                        .build())
                .reviewers(hasOnlyItem(pullRequestParticipant()
                        .name(is(testUser))
                        .role(is("REVIEWER"))
                        .build()));
        final Matcher<PullRequestStatus> openPRMatcher = prMatcherBuilder.build();

        PullRequestStatus forkTestRepoAndCreateTestPR() {
            return forkTestRepoAndCreateTestPR(null);
        }

        PullRequestStatus forkTestRepoAndCreateTestPR(@Nullable final AvatarRequest avatarRequest) {
            forkTestRepository();

            assertThat("no PRs initially",
                    service.getPullRequestsByRepository(projectKey, sourceRepo, sourceBranch, OUTGOING, OPEN, NEWEST, 0, 5),
                    page(PullRequestStatus.class)
                            .size(is(0))
                            .build());

            // create a PR
            final PullRequestStatus pullRequest = service.createPullRequest("TEST_PR", "TEST DESC",
                    fromRef, toRef, ImmutableList.of(testUser), avatarRequest);
            assertThat("PR created correctly", pullRequest, openPRMatcher);
            return pullRequest;
        }
    }
}
