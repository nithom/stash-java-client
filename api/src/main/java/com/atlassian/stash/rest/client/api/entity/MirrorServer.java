package com.atlassian.stash.rest.client.api.entity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Basic representation of a repository mirror
 */
public class MirrorServer {
    private final String selfUrl;
    private final String id;
    private final String name;
    private final boolean enabled;

    @Nullable
    public String getSelfUrl() {
        return selfUrl;
    }

    @Nonnull
    public String getId() {
        return id;
    }

    @Nonnull
    public String getName() {
        return name;
    }

    public MirrorServer(String selfUrl, String id, String name, boolean enabled) {
        this.selfUrl = selfUrl;
        this.id = id;
        this.name = name;
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
