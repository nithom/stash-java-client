package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;

/**
 * SSH public key for repository
 */
public class RepositorySshKey extends SshKey {
    private final Repository repository;
    private final String permission;

    public RepositorySshKey(final long id, final String text, final String label, final Repository repository, final String permission) {
        super(text, label, id);
        this.repository = repository;
        this.permission = permission;
    }

    /**
     * @return repository the key belongs to
     */
    public Repository getRepository() {
        return repository;
    }

    /**
     * @return permission on the repository for this ssh key
     */
    public String getPermission() {
        return permission;
    }

    @Override
    protected Objects.ToStringHelper toStringHelper() {
        return super.toStringHelper()
                .add("repository", repository)
                .add("permission", permission);
    }
}
