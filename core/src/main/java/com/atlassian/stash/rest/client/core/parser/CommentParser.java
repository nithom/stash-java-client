package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Comment;
import com.google.common.base.Preconditions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.annotation.Nullable;
import java.util.function.Function;

public class CommentParser implements Function<JsonElement, Comment> {
    @Override
    public Comment apply(@Nullable final JsonElement comment) {
        final JsonObject jsonObject = Preconditions.checkNotNull(comment, "comment").getAsJsonObject();

        final long id = Preconditions.checkNotNull(jsonObject.get("id"), "id").getAsLong();
        final long version = Preconditions.checkNotNull(jsonObject.get("version"), "version").getAsLong();
        final String text = Preconditions.checkNotNull(jsonObject.get("text"), "text").getAsString();

        return new Comment(id, version, text);
    }
}