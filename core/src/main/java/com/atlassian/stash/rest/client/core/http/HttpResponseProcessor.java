package com.atlassian.stash.rest.client.core.http;

import com.atlassian.stash.rest.client.api.StashException;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * Handler responsible for processing response. It's passed as parameter in {@link HttpExecutor#execute(HttpRequest, HttpResponseProcessor)}
 * @param <T> Result type of {@link HttpExecutor#execute(HttpRequest, HttpResponseProcessor)}
 */
public interface HttpResponseProcessor<T> {

    /**
     * Starts processing of response. Callee MUST NOT store reference to response or any of its children
     * especially response body stream because it will be closed after return from this method
     *
     * @param response    Response data
     * @return Result which will be returned as result of {@link HttpExecutor#execute(HttpRequest, HttpResponseProcessor)}
     *      call
     * @throws StashException
     * @throws IOException
     */
    T process(@Nonnull HttpResponse response) throws StashException, IOException;

}
