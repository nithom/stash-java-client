package com.atlassian.stash.rest.client.core.http;

import com.google.common.base.Throwables;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.function.BiConsumer;

/**
 * Simple UriBuilder, provided only to avoid external dependencies
 */
@NotThreadSafe
public class UriBuilder {
    private final String path;
    private StringBuilder queryParams = new StringBuilder();

    private UriBuilder(@Nonnull final String pathFormat, @Nonnull final String... unencodedPathSegments) {
        this.path = encodePath(String.format(pathFormat, (Object[]) unencodedPathSegments));
    }

    private static String encodePath(final String path) {
        try {
            return new URI(null, null, path, null).getRawPath();
        } catch (URISyntaxException e) {
            throw Throwables.propagate(e);
        }
    }

    /**
     * factory method, generates new builder for given path.
     *
     * @param pathFormat            path format where path segments can be parametrised with <code>%s</code> like in
     *                              {@link String#format(String, Object...)}.
     * @param unencodedPathSegments unencoded path segments that will be encoded and inserted into <code>%s</code>
     *                              elements of the path format provided.
     * @return new {@link UriBuilder} instance.
     */
    @Nonnull
    public static UriBuilder forPath(@Nonnull final String pathFormat, @Nonnull final String... unencodedPathSegments) {
        return new UriBuilder(pathFormat, unencodedPathSegments);
    }

    /**
     * adds query parameter if given value is not null, encoding both name and value if necessary.
     *
     * @param name  name of parameter.
     * @param value value of the parameter.
     * @return this {@link UriBuilder} instance.
     */
    @Nonnull
    public UriBuilder addQueryParam(@Nonnull final String name, @Nullable final String value) {
        if (null == value) {
            return this;
        }
        if (queryParams.length() == 0) {
            queryParams.append('?');
        } else {
            queryParams.append('&');
        }
        queryParams.append(encodeQueryParam(name))
                .append('=')
                .append(encodeQueryParam(value));
        return this;
    }

    /**
     * encodeQueryParam given object with provided function.
     *
     * @param t      object to be encoded.
     * @param mapper operation that would be given object to encodeQueryParam and this {@link UriBuilder} instance, to map this
     *               object into query parameters.
     * @param <T>    any type.
     * @return this {@link UriBuilder} instance.
     */
    @Nonnull
    public <T> UriBuilder encodeQueryParam(@Nullable final T t, @Nonnull final BiConsumer<UriBuilder, T> mapper) {
        if (null != t) {
            mapper.accept(this, t);
        }
        return this;
    }

    /**
     * converts current path and query params into {@link String}.
     *
     * @return encoded path and query params.
     */
    public String build() {
        return path + queryParams.toString();
    }

    private static String encodeQueryParam(@Nonnull final String unencoded) {
        try {
            return URLEncoder.encode(unencoded, StandardCharsets.UTF_8.name()).replace("+", "%20");
        } catch (final UnsupportedEncodingException ex) {
            throw new RuntimeException("UTF-8 not supported", ex);
        }
    }
}
