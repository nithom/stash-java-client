package com.atlassian.stash.rest.client.core.entity;


import com.google.gson.JsonObject;

public class StashCreateRepositoryRequest {

    private final String name;
    private final String scmId;
    private final boolean forkable;

    public StashCreateRepositoryRequest(final String name, final String scmId, final boolean forkable) {
        this.name = name;
        this.scmId = scmId;
        this.forkable = forkable;
    }

    public JsonObject toJson() {
        JsonObject req = new JsonObject();

        req.addProperty("name", name);
        req.addProperty("scmId", scmId);
        req.addProperty("forkable", forkable);

        return req;
    }
}
