package com.atlassian.stash.rest.client.core;

import com.atlassian.stash.rest.client.core.http.HttpExecutor;
import com.atlassian.stash.rest.client.core.http.HttpRequest;
import com.atlassian.stash.rest.client.core.http.HttpResponse;
import com.atlassian.stash.rest.client.core.http.HttpResponseProcessor;
import com.google.common.collect.ImmutableMap;
import org.mockito.Mockito;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Mock for multiple http response of HttpExecutor
 */
public class HttpExecutorMock {
    private Queue<HttpResponse> responses;

    public HttpExecutorMock(HttpExecutor httpExecutor) {
        responses = new LinkedList<>();
        Mockito.doAnswer(
                invocation -> {
                    HttpResponseProcessor responseProcessor = (HttpResponseProcessor) invocation.getArguments()[1];
                    if (responses.isEmpty()) {
                        throw new IllegalStateException("There is no HttpResponse to respond with.");
                    }
                    HttpResponse response = responses.remove();
                    return responseProcessor.process(response);
                }
        ).when(httpExecutor).execute(Mockito.any(HttpRequest.class), Mockito.any(HttpResponseProcessor.class));
    }

    public static HttpExecutorMock from(HttpExecutor executor) {
        return new HttpExecutorMock(executor);
    }

    public HttpExecutorMock nextResponse(int statusCode, @Nonnull String body) {
        responses.add(new HttpResponse(
                statusCode,
                Integer.toString(statusCode),
                ImmutableMap.of(),
                new ByteArrayInputStream(body.getBytes(StandardCharsets.UTF_8))
        ));
        return this;
    }

    public HttpExecutorMock nextResponse(int statusCode) {
        return nextResponse(statusCode, "");
    }
}
